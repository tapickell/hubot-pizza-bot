
util = require 'util'
lodash = require 'lodash'
parseAddress = require 'parse-address-string'

util_options = {showHidden: false, depth: null}

api_url = process.env.API_URL
admin_url = process.env.ADMIN_URL

module.exports = (robot) ->

  update_brain = (key, data) ->
    robot.logger.info "setting updated data in brain #{util.inspect data}"
    robot.brain.set "#{key}", lodash.merge(robot.brain.get("#{key}"), data)
    return get_from_brain("#{key}")

  get_from_brain = (key) ->
    updated_data = robot.brain.get("#{key}") or {}
    robot.logger.info "getting data from brain #{util.inspect updated_data}"
    return updated_data

  message_member = (order_id, host, member_id) ->
    robot.logger.info "inviting #{member_id} to @#{host.real_name}'s Pizza Party"
    robot.messageRoom member_id, "Would you like a pizza? respond either 'yes please #{order_id}' or 'no thank you #{order_id}'"


  # 1 Start a pizza party from channel
    # TODO
    # check host team_id or whatever denotes their groups
    # if not a match
      # robot.messageRoom host.id, "I'm sorry #{host.real_name}, I can't let you do that..."
    # else   carry on...
    # Actually start party in API
  robot.respond /pizzaparty/i, (res) ->
    host = res.envelope.user
    data = JSON.stringify({
        hostId: host.id
      })
    robot.http("#{api_url}/v1/party")
        .header('Content-Type', 'application/json')
        .post(data) (err, response, body) ->
          if err
            robot.messageRoom host.id, "Encountered an error : #{err}"
            return

          data = JSON.parse body
          order_id = data._id
          robot.messageRoom host.id, "So you wan't to start a party? #{admin_url}/#{order_id}"
          res.send ":partyparrot: Host #{host.real_name} is starting a Pizza Party!!! :partyparrot:"
          channel_members = res.envelope.message.rawMessage.channel.members
          party_data = {party: data, members: channel_members, attending: [], declined: [], ordered: []}
          robot.logger.info "setting party data in brain for party #{order_id} with #{util.inspect party_data}"
          updated_party_data = update_brain("party_data", {"#{order_id}": party_data})
          message_member order_id, host, member for member in channel_members
          robot.messageRoom host.id, ":partyparrot: channel members have been notified of the Pizza Party order number #{order_id}!!! :partyparrot:"

  # 2A YES Response for party invitation
  robot.respond /yes please (.*)/i, (res) ->
    order_id = res.match[1]
    user = res.envelope.user
    robot.logger.info "User #{user.real_name} accepted the party invitation for #{order_id}"
    party_data = get_from_brain("party_data")
    order = party_data[order_id]
    if !order
      robot.messageRoom user.id, "That party #{order_id} does not exist in storage #{util.inpsect party_data} :confused:"
      return

    is_member = order.members.includes(user.id)
    if !is_member
      robot.messageRoom user.id, "I am very sorry but you are not invited to that party #{order_id} :cry:"
      return

    already_declined = order.declined.includes(user.id)
    if already_declined
      robot.logger.info "updating from declined to attending member #{user.id}"
      updated_declined = order.declined.slice(order.declined.indexOf(user.id)+1)
      robot.logger.info "Removed #{user.id} from #{util.inspect order.declined}, for update #{util.inspect updated_declined}"
      updated_party = {party: order.party, members: order.members, attending: order.attending, declined: updated_declined, ordered: order.ordered}
      updated_party_data = update_brain("party_data", {"#{order_id}": updated_party})

    already_attending = order.attending.includes(user.id)
    if !already_attending
      updated_party = {party: order.party, members: order.members, attending: order.attending.concat(user.id), declined: order.declined, ordered: order.ordered}
      updated_party_data = update_brain("party_data", {"#{order_id}": updated_party})

    robot.messageRoom user.id, ":partyparrot: So you want to party with us!!! :pizza:"

    addresses = get_from_brain("addresses") or {}
    already_have_address = Object.keys(addresses).includes(user.id)
    if already_have_address
      robot.messageRoom user.id, "Thank you we already have your address in the system as #{util.inspect addresses[user.id]}, if you would like to update the address respond with 'for order #{order_id} add my address {Street, City, ST, PostalCode}'"
      robot.messageRoom user.id, "if you are ready to order respond with 'what are my options for order #{order_id}'"
    else
      robot.messageRoom user.id, "It seems we do not have a current address for delivery, to add the address respond with 'for order #{order_id} add my address {Street, City, ST, PostalCode}'"

  # 2B NO Response for party invitation
  robot.respond /no thank you (.*)/i, (res) ->
    order_id = res.match[1]
    user = res.envelope.user
    robot.logger.info "User #{user.real_name} declined to party with you"
    party_data = get_from_brain("party_data")
    order = party_data[order_id]

    if !order
      robot.messageRoom user.id, "That party #{order_id} does not exist in storage #{util.inspect party_data} :confused:"
      return

    is_member = order.members.includes(user.id)
    if !is_member
      robot.messageRoom user.id, "I am very sorry but you are not invited to that party #{order_id} :cry:"
      return

    already_attending = order.attending.includes(user.id)
    if already_attending
      robot.logger.info "updating from attending to declined member #{user.id}"
      updated_party = {party: order.party, members: order.members, attending: order.attending.slice(order.attending.indexOf(user.id)+1), declined: order.declined, ordered: order.ordered}
      updated_party_data = update_brain("party_data", {"#{order_id}": updated_party})

    already_declined = order.declined.includes(user.id)
    if !already_declined
      updated_party = {party: order.party, members: order.members, attending: order.attending, declined: order.declined.concat(user.id), ordered: order.ordered}
      updated_party_data = update_brain("party_data", {"#{order_id}": updated_party})

    robot.messageRoom user.id, "Sorry to hear that, maybe you will party with us next time."

  # 3 adding address to brain for order
  robot.respond /for order (.*) add my address (.*)/i, (res) ->
    order_id = res.match[1]
    address = res.match[2]
    user = res.envelope.user
    addresses = get_from_brain("addresses")
    robot.logger.info "User #{user.real_name} would like to add address: #{address} to #{util.inspect addresses}"
    already_have_address = Object.keys(addresses).includes(user.id)
    if already_have_address
      robot.messageRoom user.id, "Thank you we already have your address in the system as #{util.inspect addresses[user.id]}, we will update with the new address you provided"

    parseAddress address, (err, addressObj) ->
      if err
        robot.messageRoom user.id, "Encountered an error : #{err}"
        return

      updated_addresses = update_brain("addresses", {"#{user.id}": addressObj})
      robot.messageRoom user.id, "Thank you we have your address in the system as #{util.inspect updated_addresses[user.id]}"

      data = JSON.stringify({
          "zip": addressObj.postal_code,
          "stateAbbrev": addressObj.state,
          "city": addressObj.city,
          "streetAddress": addressObj.street_address1
        })
      robot.http("#{api_url}/v1/orderLocation")
          .header('Content-Type', 'application/json')
          .post(data) (err, response, body) ->
            if err
              robot.messageRoom user.id, "Encountered an error : #{err}"
              return

            if body == "true"
              robot.messageRoom user.id, "Address is valid for delivery #{body}"
              robot.messageRoom user.id, "if you are ready to order respond with 'what are my options for order #{order_id}'"
            else
              robot.messageRoom user.id, "Address is NOT valid for delivery #{body}"
              robot.messageRoom user.id, "I am sorry please reach out to the party host for help."

  # 4 give user the pizza options
  robot.respond /what are my options for order (.*)/i, (res) ->
    order_id = res.match[1]
    user = res.envelope.user
    robot.messageRoom user.id, "Welcome to the party, what type of pizza would you like? 1) Veggie 2) Cheese 3) Pepperoni 4) Combo "
    robot.messageRoom user.id, "please respond with 'for order #{order_id} order me {enter pizza choice number here} '"

  # 5 Placing order once accepting party invitation
  robot.respond /for order (.*) order me (.*)/i, (res) ->
    order_id = res.match[1]
    pizza_id = res.match[2]
    user = res.envelope.user
    pizzas = ["Veggie", "Cheese", "Pepperoni", "Combo"]
    pizza = pizzas.splice(pizza_id-1, 1).pop()
    full_name = user.real_name

    party_data = get_from_brain("party_data")
    order = party_data[order_id]
    robot.logger.info "getting party data in brain for party #{order_id} with #{util.inspect party_data}"

    if !order
      robot.messageRoom user.id, "That party #{order_id} does not exist in storage #{party_data} :confused:"
      return

    is_member = order.members.includes(user.id)
    if !is_member
      robot.messageRoom user.id, "I am very sorry but you are not invited to that party #{order_id} :cry:"
      return

    already_declined = order.declined.includes(user.id)
    if already_declined
      robot.logger.info "user placing order has previously declined to attend member #{user.id}"
      robot.messageRoom user.id, "I am very sorry but you are not attending that party #{order_id} :cry:"
      return

    addresses = get_from_brain("addresses")
    user_address = addresses[user.id]
    data = JSON.stringify({
        "guestId": user.id,
        "pizzaType": pizza,
        "email": user.profile.email,
        "firstname": full_name.substring(0, full_name.indexOf(" ")),
        "lastname": full_name.substring(full_name.indexOf(" ")+1),
        "address": {
            "zip": user_address.postal_code,
            "city": user_address.city,
            "streetAddress": user_address.street_address1,
            "stateAbbrev": user_address.state
        }
      })

    robot.logger.info "User #{util.inspect user} for #{order_id} with order data #{util.inspect data}"
    already_ordered = order.ordered.includes(user.id)
    if already_ordered
      robot.logger.info "user placing order has previously ordered #{user.id}"
      updated_party = {party: order.party, members: order.members, attending: order.attending, declined: order.declined, ordered: order.ordered.slice(order.ordered.indexOf(user.id)+1)}
      updated_party_data = update_brain("party_data", {"#{order_id}": updated_party})
      updated_order = updated_party_data[order_id]
      robot.messageRoom user.id, "You have an order in memory, I will update it with your new selection"
      robot.http("#{api_url}/v1/party/#{order_id}/guest/#{user.id}")
          .header('Content-Type', 'application/json')
          .put(data) (err, response, body) ->
            if err
              robot.messageRoom host.id, "Encountered an error : #{err}"
              return

            data_body = JSON.parse body
            robot.logger.info "Added guest to party #{order_id}, #{util.inspect data_body}"
            updated_party = {party: updated_order.party, members: updated_order.members, attending: updated_order.attending, declined: updated_order.declined, ordered: updated_order.ordered.concat(user.id)}
            updated_party_data = update_brain("party_data", {"#{order_id}": updated_party})
            robot.logger.info "User #{user.real_name} is ordering a #{pizza} pizza for #{order_id} "
            robot.messageRoom user.id, "Cool, #{user.real_name}, I got you down for a #{pizza} pizza for order number #{order_id}"
    else
      robot.http("#{api_url}/v1/party/#{order_id}/guest")
          .header('Content-Type', 'application/json')
          .post(data) (err, response, body) ->
            if err
              robot.messageRoom host.id, "Encountered an error : #{err}"
              return

            data_body = JSON.parse body
            robot.logger.info "Added guest to party #{order_id}, #{util.inspect data_body}"
            updated_party_data = get_from_brain("party_data")
            updated_order = updated_party_data[order_id]
            updated_party = {party: updated_order.party, members: updated_order.members, attending: updated_order.attending, declined: updated_order.declined, ordered: updated_order.ordered.concat(user.id)}
            updated_party_data = update_brain("party_data", {"#{order_id}": updated_party})
            robot.logger.info "User #{user.real_name} is ordering a #{pizza} pizza for #{order_id} "
            robot.messageRoom user.id, "Cool, #{user.real_name}, I got you down for a #{pizza} pizza for order number #{order_id}"

    robot.logger.info "getting updated party data in brain for party #{order_id} with #{util.inspect get_from_brain("party_data")}"

    # res.envelope.room 'C5SBFRB5Z'
    # res.envelope.user {}
    # res.envelope.user.id  'U81SSJGMU'
    # res.envelope.user.team_id  'T5SGEK2MT'
    # res.envelope.user.real_name 'Todd Pickell'
    # res.envelope.user.is_bot  false
    # res.envelope.text '@pizzabot-hubot pizzaparty'
    # res.envelope.raw_message.channel.members   [ 'U5SBHJQKV'...]

