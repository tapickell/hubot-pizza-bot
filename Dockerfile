FROM node:6.6.0

WORKDIR /opt/bot
COPY package.json ${WORKDIR}
RUN npm install
COPY . ${WORKDIR}

ENV API_URL "https://pizza.dev.surgeforward.com/api"
ENV ADMIN_URL "https://pizza.dev.surgeforward.com"
ENV HUBOT_PORT 8080
ENV HUBOT_ADAPTER slack
ENV HUBOT_NAME hubot-pizza-bot
ENV HUBOT_SLACK_TOKEN "xoxb-285774317972-09xoHUqPwQjUts0oGniY7PaZ"
ENV PORT ${HUBOT_PORT}

EXPOSE ${HUBOT_PORT}
EXPOSE 3001

CMD "bin/hubot"
