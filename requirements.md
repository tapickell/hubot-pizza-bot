#### This repo is for the Slack bot that will interact with the SPP API,

###### Bot Requirements

* [X] Responds to `/pizzaparty` in `#channel`
* [X] Checks user info w/ Slack for group association
* [X] If the host is not a member of the `@pizzahosts` slack group
they get a DM letting them know and cancels the request
* "I'm sorry Dave, I can't let you do that..."
* [ ] Bot connects to API to start party and get URL for
admin portal
* `POST /party`
* [X] Bot DM's pizzaparty host with web admin
URL for just this order
portal

* [X] Bot posts in `#channel`
“:partyparrot: Host is starting a
Pizza Party!!! :partyparrot:”

* [ ] Bot DM’s all users in `#channel`
"Would you like a pizza?"
* [ ] If No, say something and
EXIT
* [ ] If Yes, continue with
placing order
* [ ] gets users address
* "What is your
zip?"
* "What is
your
state?"
* "What
is
your
city?"
* "What
is
your
street
address?"
* [
]
Validates
user
location
with
API
"We’ll
see
if
you’re
in
range
of
a
pizza
dispensary"
* [
]
If
No,
  say
  something
  and
  EXIT
  * [
  ]
  If
  Yes,
  say
  “you’re
  in
  range”
  * [
  ]
  `POST
  /party/guest`
  adds
  user
  to
  party
  * [
  ]
  Continue
  with
  getting
  order
  from
  user
  * "What
  kind
  of
  pizza
  would
  you
  like?
  1)
  Veggie
  2)
  Cheese
  3)
  Pepperoni
  4)
  Combo"
  * [
  ]
  Validate
  the
  selection,
  add
  it
  to
  the
  pending
  order
  * [
  ]
  Order
  needs
  to
  contain/link
  to
  user
  info
  * [
  ]
  `POST
  /party/:id/order`
  add
  a
  new
  order
  to
  the
  party
  * [
  ]
  Once
  order
  is
  confirmed,
  bot
  sends
  message
  to
  channel
  “PIZZAS
  ARE
  ON
  THE
  WAY
  TMNT.gif”
  * [
  ]
  Order
  is
  confirmed
  from
  Admin
  UI
  via
  API,
  needs
  to
  notify
  slack
  bot
  of
  the
  updated
  order
  status
  to
  trigger
  this
  confirmation
  message
  * [
  ]
  If
  order
  is
  denied,
  bot
  sends
  DM
  to
  host
  user
  w/
  denial


######
  Possible
  features
  * timer
  feature
  that
  counts
  down
  from
  party
  start
  giving
  allotted
  time
  for
  users
  to
  enter
  orders,
  could
  prompt
  in
  `#channel`
  at
  intervals
  "5
  minutes
  left
  to
  place
  order"
